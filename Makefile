CFLAGS=-Wall -g

all: clean uppercase

clean:
	find . -maxdepth 1 -type f -executable -delete

run: all
	gdb uppercase

build: clean
	${CC} -Wall -O2 -o uppercase uppercase.c
	x86_64-w64-mingw32-gcc -o uppercase.exe uppercase.c
