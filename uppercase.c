#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char* argv[]) {
   char* result;
   unsigned int size = 1; // 1 because of the newline char
   unsigned short numberOfWords = argc;
   
   // if input is coming from command line arguments
   if(argc > 1) {
      // allocating memory, finding the total malloc size
      for(int i=1; i < numberOfWords; ++i) {
         if(i != 1) size += 1; // for the whitespace char
         size += strlen(argv[i]);
      }

      // concatenate input of the words
      if((result = malloc(size+1*sizeof(char))) != NULL) {
         // assure that result will have a terminating char
         result[0] = '\0';
         // concatenating string
         for(int i=1; i < numberOfWords; ++i) {
            if(i != 1) strcat(result, " "); // add the whitespace char
            strcat(result, argv[i]);
         }
         // assure output ending with a newline
         strcat(result, "\n");
      } else {
         fprintf(stderr, "lowercase: malloc failed!\n");
      }
   } else {
      char buf[BUFSIZ];
      fgets(buf, sizeof(buf), stdin);
      result = buf;
   }

   // converting all letters to upper case
   for(int i = 0; result[i]; ++i) {
      result[i] = toupper(result[i]);
   }

   // printing out
   fprintf(stdout, "%s", result);

   return 0;
}
